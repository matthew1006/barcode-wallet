/****************************************************************************
** Meta object code from reading C++ file 'file.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../build/src/plugins/cordova-plugin-file/file.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'file.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_File_t {
    QByteArrayData data[37];
    char stringdata[385];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_File_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_File_t qt_meta_stringdata_File = {
    {
QT_MOC_LITERAL(0, 0, 4), // "File"
QT_MOC_LITERAL(1, 5, 17), // "requestFileSystem"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 4), // "scId"
QT_MOC_LITERAL(4, 29, 4), // "ecId"
QT_MOC_LITERAL(5, 34, 4), // "type"
QT_MOC_LITERAL(6, 39, 4), // "size"
QT_MOC_LITERAL(7, 44, 25), // "resolveLocalFileSystemURI"
QT_MOC_LITERAL(8, 70, 12), // "getDirectory"
QT_MOC_LITERAL(9, 83, 7), // "getFile"
QT_MOC_LITERAL(10, 91, 10), // "parentPath"
QT_MOC_LITERAL(11, 102, 5), // "rpath"
QT_MOC_LITERAL(12, 108, 7), // "options"
QT_MOC_LITERAL(13, 116, 11), // "readEntries"
QT_MOC_LITERAL(14, 128, 3), // "uri"
QT_MOC_LITERAL(15, 132, 9), // "getParent"
QT_MOC_LITERAL(16, 142, 6), // "copyTo"
QT_MOC_LITERAL(17, 149, 6), // "source"
QT_MOC_LITERAL(18, 156, 14), // "destinationDir"
QT_MOC_LITERAL(19, 171, 7), // "newName"
QT_MOC_LITERAL(20, 179, 6), // "moveTo"
QT_MOC_LITERAL(21, 186, 15), // "getFileMetadata"
QT_MOC_LITERAL(22, 202, 11), // "getMetadata"
QT_MOC_LITERAL(23, 214, 6), // "remove"
QT_MOC_LITERAL(24, 221, 17), // "removeRecursively"
QT_MOC_LITERAL(25, 239, 5), // "write"
QT_MOC_LITERAL(26, 245, 8), // "position"
QT_MOC_LITERAL(27, 254, 6), // "binary"
QT_MOC_LITERAL(28, 261, 10), // "readAsText"
QT_MOC_LITERAL(29, 272, 8), // "encoding"
QT_MOC_LITERAL(30, 281, 10), // "sliceStart"
QT_MOC_LITERAL(31, 292, 8), // "sliceEnd"
QT_MOC_LITERAL(32, 301, 13), // "readAsDataURL"
QT_MOC_LITERAL(33, 315, 17), // "readAsArrayBuffer"
QT_MOC_LITERAL(34, 333, 18), // "readAsBinaryString"
QT_MOC_LITERAL(35, 352, 8), // "truncate"
QT_MOC_LITERAL(36, 361, 23) // "_getLocalFilesystemPath"

    },
    "File\0requestFileSystem\0\0scId\0ecId\0"
    "type\0size\0resolveLocalFileSystemURI\0"
    "getDirectory\0getFile\0parentPath\0rpath\0"
    "options\0readEntries\0uri\0getParent\0"
    "copyTo\0source\0destinationDir\0newName\0"
    "moveTo\0getFileMetadata\0getMetadata\0"
    "remove\0removeRecursively\0write\0position\0"
    "binary\0readAsText\0encoding\0sliceStart\0"
    "sliceEnd\0readAsDataURL\0readAsArrayBuffer\0"
    "readAsBinaryString\0truncate\0"
    "_getLocalFilesystemPath"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_File[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    4,  109,    2, 0x0a /* Public */,
       7,    3,  118,    2, 0x0a /* Public */,
       8,    5,  125,    2, 0x0a /* Public */,
       9,    5,  136,    2, 0x0a /* Public */,
      13,    3,  147,    2, 0x0a /* Public */,
      15,    3,  154,    2, 0x0a /* Public */,
      16,    5,  161,    2, 0x0a /* Public */,
      20,    5,  172,    2, 0x0a /* Public */,
      21,    3,  183,    2, 0x0a /* Public */,
      22,    3,  190,    2, 0x0a /* Public */,
      23,    3,  197,    2, 0x0a /* Public */,
      24,    3,  204,    2, 0x0a /* Public */,
      25,    6,  211,    2, 0x0a /* Public */,
      28,    6,  224,    2, 0x0a /* Public */,
      32,    5,  237,    2, 0x0a /* Public */,
      33,    5,  248,    2, 0x0a /* Public */,
      34,    5,  259,    2, 0x0a /* Public */,
      35,    4,  270,    2, 0x0a /* Public */,
      36,    3,  279,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::UShort, QMetaType::ULongLong,    3,    4,    5,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QVariantMap,    3,    4,    2,    2,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QVariantMap,    3,    4,   10,   11,   12,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,   14,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,   14,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    4,   17,   18,   19,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    4,   17,   18,   19,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::ULongLong, QMetaType::Bool,    3,    4,    2,    2,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    2,   29,   30,   31,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    2,   30,   31,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    2,   30,   31,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    2,   30,   31,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::ULongLong,    3,    4,    2,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    3,    4,    2,

       0        // eod
};

void File::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        File *_t = static_cast<File *>(_o);
        switch (_id) {
        case 0: _t->requestFileSystem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< unsigned short(*)>(_a[3])),(*reinterpret_cast< unsigned long long(*)>(_a[4]))); break;
        case 1: _t->resolveLocalFileSystemURI((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 2: _t->getDirectory((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QVariantMap(*)>(_a[5]))); break;
        case 3: _t->getFile((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QVariantMap(*)>(_a[5]))); break;
        case 4: _t->readEntries((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 5: _t->getParent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 6: _t->copyTo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5]))); break;
        case 7: _t->moveTo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5]))); break;
        case 8: _t->getFileMetadata((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 9: _t->getMetadata((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 10: _t->remove((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 11: _t->removeRecursively((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 12: _t->write((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< unsigned long long(*)>(_a[5])),(*reinterpret_cast< bool(*)>(_a[6]))); break;
        case 13: _t->readAsText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 14: _t->readAsDataURL((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 15: _t->readAsArrayBuffer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 16: _t->readAsBinaryString((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 17: _t->truncate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< unsigned long long(*)>(_a[4]))); break;
        case 18: _t->_getLocalFilesystemPath((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObject File::staticMetaObject = {
    { &CPlugin::staticMetaObject, qt_meta_stringdata_File.data,
      qt_meta_data_File,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *File::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *File::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_File.stringdata))
        return static_cast<void*>(const_cast< File*>(this));
    return CPlugin::qt_metacast(_clname);
}

int File::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CPlugin::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
