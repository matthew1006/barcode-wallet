def EAN13Check (number):
	sum = 0
	weight = 3
	while number > 0:
		sum += weight * (number % 10)
		number /= 10
		weight = 3 if weight is 1 else 1

	return 10 - (sum % 10)

print (EAN13Check (590123412345))
