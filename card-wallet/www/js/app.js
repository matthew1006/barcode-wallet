/*
Copyright (c) 2015, Matthew Edwards
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
*/

var UI = null;

window.onload = function () {

    function addClass(elem, className) {
        elem.className += ' ' + className;
    };

    function removeClass(elem, className) {
        elem.className = elem.className.replace(className, '');
    };

    UI = new UbuntuUI();
    UI.init();

    // Detect if Cordova script is uncommented or not and show the appropriate status.
    var hasCordovaScript = false;
    var scripts = [].slice.call(document.querySelectorAll('script'));
    scripts.forEach(function (element) {
        var attributes = element.attributes;
        if (attributes && attributes.src && attributes.src.value.indexOf('cordova.js') !== -1) {
            hasCordovaScript = true;
        }
    });

    var cordovaLoadingIndicator = document.querySelector('.load-cordova');
    if (!hasCordovaScript) {
        removeClass(document.querySelector('.ko-cordova'), 'is-hidden');
        addClass(cordovaLoadingIndicator, 'is-hidden');
    }

    // Add an event listener that is pending on the initialization
    //  of the platform layer API, if it is being used.
    document.addEventListener("deviceready", function() {
        if (console && console.log) {
            console.log('Platform layer API ready');

        }
        removeClass(document.querySelector('.ok-cordova'), 'is-hidden');
        addClass(cordovaLoadingIndicator, 'is-hidden');
    }, false);

    var cardType = UI.optionselector("Card-Type", false);

    cardType.onClicked (function (e)
    {
        currentCardType = parseInt (e.values);

        document.getElementById("Code-Even-Error").style.display = "none";
        document.getElementById("Code-13-Error").style.display = "none";

        switch (currentCardType)
        {
        case 0:
        case 6:
            document.getElementById ("Edit-Card-Value").type = "text";
            break;
        default:
            document.getElementById ("Edit-Card-Value").type = "number";
            break;
        }
    });

    UI.button("Edit-Button").click (editCard);
    UI.button("New-Button").click (newCard);
    UI.button("Delete-Button").click (deleteCard);
    UI.button("Save-Button").click (saveCard);

    applyLocalisation ();

    UI.pagestack.push ("Card-List-Page");
    populateCardList ();
};

var cards = null;
var currentCardIndex = -1;

function populateCardList ()
{
    loadCards ();

    refreshCardList ();
}

function refreshCardList ()
{
    var filter = document.getElementById ("card-filter").value.toUpperCase();
    var cardList = UI.list ('[id="card-list"]');

    cardList.removeAllItems();

    for (var i = 0; i < cards.cards.length; i++)
    {
        var cardName = cards.cards[i].name;
        if (filter.length === 0 || cardName.toUpperCase ().includes (filter))
        {
            addCardToList (cardList, i, cardName);
        }
    }
}

function addCardToList (cardList, index, cardName)
{
    cardList.append (cardName, "", index.toString (), showCardDetailsTab, index);
}

function showCardDetailsTab (button, index)
{
    if (cards === null) return;

    currentCardIndex = index;
    refreshCardDetails ();
    UI.pagestack.push("card-page");
}

function refreshCardDetails ()
{
    var name = cards.cards[currentCardIndex].name;
    var value = cards.cards[currentCardIndex].value;
    var type = cards.cards[currentCardIndex].type;

    var page = UI.page ("card-page");
    page.element().setAttribute ("data-title", name);

    var Barcode = document.getElementById ("Barcode");
    var Barcode_Value = document.getElementById ("Barcode_Value");

    var code = stringToBarcode (value, type);
    Barcode.innerHTML = code;
    Barcode_Value.innerHTML = value;

	switch (type)
	{
		case 3:
        case 4:
			Barcode.style.fontFamily = "ean13";
            break;
        
        case 5:
			Barcode.style.fontFamily = "i25";
            break;

        case 6:
            Barcode.style.fontFamily = "code39";
            break;
			
		default:
			Barcode.style.fontFamily = "code128";	
            break;
    }

    var fontSize = 20.5;
    Barcode.style.fontSize = fontSize + "vw";

    console.log (window.innerWidth * 1.5);
    while (fontSize > 0 && calculateBarcodeBiggerThanScreen (code, window.getComputedStyle (Barcode), window.innerWidth))
    {
        fontSize -= 1;
        Barcode.style.fontSize = fontSize + "vw";
    }
}

function calculateBarcodeBiggerThanScreen (contents, style, screenWidth)
{
    fontSize = parseFloat(style.fontSize);
    padding = parseFloat (style.paddingLeft) + parseFloat(style.paddingRight);
    var p = contents.length * 0.3 * fontSize + padding;
    return p > screenWidth * 2;
}

function stringToBarcode (str, type)
{
    switch (type)
    {
    case 0:
        return "&Ntilde;" + getCodeB(str) + "&Oacute;";

    case undefined:
    case 1:
        return "&Ograve;" + getCodeC (str) + "&Oacute;";

    case 2:
        return "&Ograve;" + getCodeGS1(str) + "&Oacute;";

    case 3:
        return ":" + getEAN13(str) + "+";

    case 4:
        return ":" + getEAN8(str) + "+";

    case 5:
        return "(" + getI2of5(str) + ")";

    case 6:
        return "*" + str.toUpperCase().replace (/[^A-Z\s\d-$%./+]/g, "") + "*";
    }
}

function calculateCheck (code, start)
{
    var CheckSum = start;
    var skips = 0;
	for (var i = 0; i < code.length; i++)
    {
        if (code[i] < 0)
        {
            skips++;
            continue;
        }

        CheckSum += code[i] * (i + 1 - skips);
    }
	return CheckSum % 103;
}

function getCharacterFromValue (v)
{
	if (v >= 0 && v <= 94)
	{
		return String.fromCharCode (v + 32);
	}
	else if (v >= 95 && v <= 102)
	{
		return String.fromCharCode (v + 100)
	}
	return "";
}

function getValueFromCharacter (v)
{
    var value = v.charCodeAt(0);
    if (value >= 32 && value <= 126)
    {
        return value - 32;
    }
    else if (value >= 195 && value <= 202)
    {
        return value - 100;
    }
    return -1;
}

function getCodeB (code)
{
    code = code.replace (/[^\s\da-zA-Z!"#$%&'()*+,-./:;<=>?@\[\\\]^_`{|}~]/g, "");
    var codeArray = [];

    for (var i = 0; i < code.length; i++)
	{
        codeArray.push (getValueFromCharacter (code[i]));
    }

    return escapeString (code + getCharacterFromValue (calculateCheck (codeArray, 104)));
}

function getCodeC (code)
{
    var codeArray = [];
    var codeString = "";

    for (var i = 0; i < code.length; i+=2)
    {
        var value = parseInt (code.substring (i, i+2));
        if (isNaN (value)) return "";
        codeArray.push (value);
        codeString += getCharacterFromValue (value);
    }

    return escapeString (codeString + getCharacterFromValue (calculateCheck (codeArray, 105)));
}

function getCodeGS1 (code)
{
    var codeArray = [102];
    var codeString = "\xCA";

    for (var i = 0; i < code.length; i+=2)
    {
        var value = parseInt (code.substring (i, i+2));
        if (isNaN (value)) return "";
        codeArray.push (value);
        codeString += getCharacterFromValue (value);
    }

    return escapeString (codeString + getCharacterFromValue (calculateCheck (codeArray, 105)));
}

function getEAN13 (code)
{
    var parityMasks = [0, 11, 13, 16, 19, 25, 28, 21, 22, 26];
    var firstDigit = parseInt (code[0]);
    var parityMask = parityMasks [firstDigit];
    var codeString = "";

    var checkSum = firstDigit;
    var weight = 3;

    for (var i = 1; i < 12; i++)
    {
        var digit = parseInt (code[i]);
        checkSum += digit * weight;
        weight = (weight === 3 ? 1 : 3);

        if (i < 7)
        {
            var parity = (32 >> (i-1)) & parityMask;
            if (parity === 0)
            {
                codeString += String.fromCharCode (65 + digit);
            }
            else
            {
                codeString += String.fromCharCode (75 + digit);
            }
        }
        else
        {
            if (i === 7)
            {
                codeString += "*";
            }
            codeString += String.fromCharCode (97 + digit);
        }
    }

    codeString += String.fromCharCode (97 + (10 - checkSum % 10) % 10);

    return escapeString (codeString);
}

function getEAN8 (code)
{
    var codeString = "";

    var checkSum = 0;
    var weight = 3;

    for (var i = 0; i < 7; i++)
    {
        var digit = parseInt (code[i]);
        checkSum += digit * weight;
        weight = (weight === 3 ? 1 : 3);

        if (i < 4)
        {
            codeString += String.fromCharCode (65 + digit);
        }
        else
        {
            if (i === 4)
            {
                codeString += "*";
            }
            codeString += String.fromCharCode (97 + digit);
        }
    }

    codeString += String.fromCharCode (97 + (10 - checkSum % 10) % 10);

    return escapeString (codeString);
}

function getI2of5 (code)
{
    if ((code.length & 1) === 1) return "";
	
	var codeString = "";
	
	for (var i = 1; i < code.length; i += 2)
	{
        var value = parseInt (code[i-1] + code[i]);

		if (value < 50)
			codeString += String.fromCharCode (value + 0x30);
		else
			codeString += String.fromCharCode (value + 0x8e);
	}

    return escapeString (codeString);
}

function editCard (index)
{
    modifyCard (translations["Edit-Page-Title"][language]);
}

function newCard ()
{
    var New_Card = new Object ();
    New_Card.name = "";
    New_Card.value= "";
    New_Card.type = 1;

    currentCardIndex = cards.cards.length;
    cards.cards.push (New_Card);

    UI.list ('[id="card-list"]').append (" ", "", currentCardIndex,  showCardDetailsTab, currentCardIndex);

    UI.pagestack.push ("card-page");
    modifyCard (translations["New-Page-Title"][language]);
    UI.button("ubuntu-pagestack-back-0").element().setAttribute ("disabled", "");
}

function modifyCard (title)
{
    if (currentCardIndex >= cards.cards.length) return;

    document.getElementById("Code-Even-Error").style.display = "none";
    document.getElementById("Code-13-Error").style.display = "none";
    document.getElementById("Code-8-Error").style.display = "none";

    var page = UI.page("edit-page");
    page.element ().setAttribute ("data-title", title);

    var cardNameInput = document.getElementById("Edit-Card-Name");
    var cardValueInput = document.getElementById("Edit-Card-Value");
    //var cardType = document.getElementById ("Card-Type");

    cardNameInput.value = cards.cards[currentCardIndex].name;
    cardValueInput.value = cards.cards[currentCardIndex].value;
    currentCardType = cards.cards[currentCardIndex].type;
    setCardTypeSelection (currentCardType);

    UI.pagestack.push ("edit-page");
}

var currentCardType = 1;
function saveCard ()
{
    var cardNameInput = document.getElementById("Edit-Card-Name");
    var cardValueInput = document.getElementById("Edit-Card-Value");
    //var cardType = document.getElementById ("Card-Type");

    if ((currentCardType === 1 || currentCardType === 2 || currentCardType === 5) && (cardValueInput.value.length & 1) === 1)
    {
        document.getElementById("Code-Even-Error").style.display = "block";
        return;
    }
    if (currentCardType === 3 && !(cardValueInput.value.length === 12 || cardValueInput.value.length === 13))
    {
        document.getElementById("Code-13-Error").style.display = "block";
        return;
    }
    if (currentCardType === 4 && !(cardValueInput.value.length === 7 || cardValueInput.value.length === 8))
    {
        document.getElementById("Code-8-Error").style.display = "block";
        return;
    }

    cards.cards[currentCardIndex].name = cardNameInput.value;
    cards.cards[currentCardIndex].value = cardValueInput.value;
    cards.cards[currentCardIndex].type = currentCardType;

    refreshCardDetails ();

    UI.list('[id="card-list"]').at(currentCardIndex+1).firstChild.innerHTML = cardNameInput.value;

    UI.pagestack.pop ();
    writeChanges ();
}

function deleteCard ()
{
    cards.cards.pop (currentCardIndex);
    refreshCardList();

    UI.pagestack.clear ();
    UI.pagestack.push ("Card-List-Page");

    writeChanges ();
}

function writeChanges ()
{
    localStorage.setItem ("cards", JSON.stringify(cards));
}

function loadCards ()
{
    var _json = localStorage.getItem ("cards");
    if (_json === null || _json === undefined)
    {
        _json = '{ "cards" : [] }';
    }

    cards = JSON.parse (_json);
}

function setCardTypeSelection (type)
{
    var optionsList = UI.optionselector ("Card-Type").optionselector_ul_li;

    if (type < optionsList.length)
    {

        var clickEvent = document.createEvent ("Event");
        clickEvent.initEvent ("click", true, true);
        var option = optionsList[type];
        option.dispatchEvent (clickEvent);
        option.dispatchEvent (clickEvent);
    }
}

function escapeString (str)
{
    str = str.replace (/&/g, "&amp;");
    str = str.replace (/</g, "&lt;");
    str = str.replace (/>/g, "&gt;");
	
	return str;
}
